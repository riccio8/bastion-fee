## Bastion Fee

# Getting Started #

1. Install Nuget packages into your application.

    ```
    Package Manager : Install-Package Bastion.Fee
    CLI : dotnet add package Bastion.Fee
	```
	```
	Package Manager : Install-Package Bastion.Fee.EntityFrameworkCore
    CLI : dotnet add package Bastion.Fee.EntityFrameworkCore
    ```

2. In the `ConfigureServices` method of `Startup.cs`.
    	```
            services.AddScoped<IFeeRepository, FeeRepository>();
	```
3. Create a migration and update the database

	```
	Package Manager : Add-Migration InitialCreate
	Package Manager : Update-Database
	```

4. If you need web api, install the Nuget package.

	```
	Package Manager : Install-Package Bastion.Fee.Api
    CLI : dotnet add package Bastion.Fee.Api
    ```