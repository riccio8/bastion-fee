﻿namespace Bastion.Fee.EntityFrameworkCore.Repositories
{
    using Bastion.Accounts;
    using Bastion.Accounts.Constants;
    using Bastion.Accounts.Interfaces;
    using Bastion.Accounts.Models;
    using Bastion.Fee.Abstractions;
    using Bastion.Fee.Stores;
    using Bastion.Helper;
    using Bastion.Users.Constants;
    using Bastion.Users.Extensions;
    using Bastion.Users.Services;
    using Microsoft.EntityFrameworkCore;

    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Bastion.Fee.EntityFrameworkCore.Contexts;
    using Bastion.Users.Models;
    using Bastion.Fee.EntityFrameworkCore.ConstFee;
    using Group = Stores.Group;
    using Bastion.Kernel;

    public class FeeRepository : Repository<FeeDbContext>, IFeeRepository
    {
        private readonly UsersRepository usersRepository;
        private readonly TransactionRepository transactionRepository;
        private readonly FeeDbContext dbContext;
        private readonly IList<Formula> formulas;
        private readonly IList<Structure> structures;
        private readonly IList<Title> titles;
        private readonly IList<Collector> collectors;
        private readonly IList<CollectorTitle> collectorsTitle;

        public FeeRepository(TransactionRepository transactionRepository,
            UsersRepository usersRepository, FeeDbContext dbContext) : base(dbContext)
        {
            this.usersRepository = usersRepository;
            this.transactionRepository = transactionRepository;
            this.formulas = this.GetEntities<Formula>().GetAwaiter().GetResult();
            this.structures = this.GetEntities<Structure>().GetAwaiter().GetResult();
            this.titles = this.GetEntities<Title>().GetAwaiter().GetResult();
            this.collectors = this.GetEntities<Collector>().GetAwaiter().GetResult();
            this.collectorsTitle = this.GetEntities<CollectorTitle>().GetAwaiter().GetResult();
            this.dbContext = dbContext;
        }

        public async Task<Formula> GetFeeFormulaAsync(string feeType, string structureId)
        {
            Structure structure = structures.First(x => x.StructureId == structureId);
            CustomValidator.CheckNotNull(structure, $"Unknown structure '{structureId}'. ({nameof(FeeRepository)}.{nameof(GetFeeAsync)})");
            Formula formula = formulas.FirstOrDefault(x => x.TitleId == feeType.ToString() && x.StructureId == structureId);
            if (formula != null)
            {
                return formula;
            }
            else
            {
                if (structure.StructureId == structure.ParentStructureId)
                {
                    throw new Exception($"Didn't create formula for default structure for '{feeType}'. ({nameof(FeeRepository)}.{nameof(GetFeeFormulaAsync)})");
                }
                return await GetFeeFormulaAsync(feeType, structure.ParentStructureId);
            }
        }

        public async Task<IList<Structure>> GetStructuresAsync()
        {
            return await dbContext.Structure
                .Include(x => x.InverseParentStructure)
                .Include(x => x.Formula).ThenInclude(x => x.Title).ThenInclude(x => x.Group)
                .ToListAsync();
        }

        public async Task<IEnumerable<FeeInfo>> GetFeeAsync(string feeType, string currencyId, Func<Account, bool> accountFilter, params string[] accountsId)
        {
            List<FeeInfo> fees = new List<FeeInfo>(accountsId.Length);
            foreach (var accountId in accountsId)
            {
                Account account = await transactionRepository.GetAccount(accountId);
                var userProfile = await usersRepository.GetUserProfile(account.UserId);
                Account feeAccount = (await transactionRepository.GetAccounts(account.UserId)).FirstOrDefault(accountFilter);
                if (feeAccount == null)
                {
                    throw new Exception($"Doesn't exist account to collect fee for user {userProfile.ToName()}. ({nameof(FeeRepository)}.{nameof(GetFeeAsync)})");
                }
                Formula formula = await GetFeeFormulaAsync(feeType, userProfile.FeeStructureId);
                FeeInfo fee = new FeeInfo(GetCollectors(userProfile.FeeStructureId, feeType, currencyId), feeAccount.AccountId, formula, feeType);
                fees.Add(fee);
            }
            return fees;
        }

        public IEnumerable<CollectorInfo> GetCollectors(string structureId, string feeType, string currencyId)
        {
            Structure structure = structures.First(x => x.StructureId == structureId);
            CustomValidator.CheckNotNull(structure, $"Unknown structure '{structureId}'. (FeeModule.FeeRepository.GetCollectors)");
            CollectorTitle collectorTitle = collectorsTitle.FirstOrDefault(x => x.StructureId == structureId && x.TitleId == feeType);
            if (collectorTitle != null)
            {
                IEnumerable<Collector> currentCollectors = collectors.Where(x => x.StructureId == structureId && x.CurrencyId == currencyId);
                return currentCollectors.Select(x => new CollectorInfo(x.AccountId, x.Value, x.Description));
            }
            else
            {
                if (structure.StructureId == structure.ParentStructureId)
                {
                    throw new Exception($"Сollector title is not filled for structure '{structureId}' and fee '{feeType}'. (FeeModule.FeeRepository.GetFCollectors, table fee.collector_title)");
                }
                else
                {
                    return GetCollectors(structure.ParentStructureId, feeType, currencyId);
                }
            }
        }

        public async Task<Dictionary<string, Formula>> GetFeeAsync(string accountId, string currencyId)
        {
            Account account = await transactionRepository.GetAccount(accountId);
            var userProfile = await usersRepository.GetUserProfile(account.UserId);
            Dictionary<string, Formula> fees = new Dictionary<string, Formula>(titles.Count);
            foreach (var title in titles)
            {
                Account feeAccount = transactionRepository.GetAccountByCurrencyId(account.UserId, currencyId);
                string feeType = title.TitleId;
                Formula formula = await GetFeeFormulaAsync(feeType, userProfile.FeeStructureId);
                fees[title.TitleId] = formula;
            }
            return fees;
        }

        public async Task<Formula> GetFeeByUserIdAsync(string feeType, string userId)
        {
            var userProfile = await usersRepository.GetUserProfile(userId);
            Formula formula = await GetFeeFormulaAsync(feeType, userProfile.FeeStructureId);
            return formula;
        }

        public async Task<decimal> GetAvailableBalanceForWithdrawAsync(string accountId, string currencyId, Func<Account, bool> accountFilter)
        {
            decimal total = await transactionRepository.GetBalance(accountId);
            Account account = await transactionRepository.GetAccount(accountId);
            var fee = await GetFeeAsync(FeeType.WDRNEU, currencyId, accountFilter, accountId);

            var feeValue = fee.Sum(x => x.FeeCalculate.Calculate(total));
            return decimal.Round(total - feeValue, account.Currency.DecimalPoints);
        }

        public async Task<FeeInfo> GetDisplayTradeFeeAsync(string userId, TradeType tradeType, string currencyId)
        {
            UserProfile userProfile = await usersRepository.GetUserProfile(userId);
            string feeType = this.GetFeeId(tradeType);
            Formula formula = await GetFeeFormulaAsync(feeType, userProfile.FeeStructureId);
            Account feeAccount = transactionRepository.GetAccountByCurrencyId(userId, currencyId);
            CustomValidator.CheckNotNull(feeAccount, $"User {userProfile.ToName()} don't have {currencyId} account to collect fee. (FeeModule.FeeRepository.{nameof(this.GetDisplayTradeFeeAsync)})");
            FeeInfo fee = new FeeInfo(this.GetCollectors(userProfile.FeeStructureId, feeType, currencyId), feeAccount.AccountId, new PercentFee(formula.Display), feeType.ToString());
            return fee;
        }

        public async Task<IEnumerable<FeeInfo>> GetRealTradeFeeAsync(string userId, TradeType tradeType, string currencyId)
        {
            UserProfile userProfile = await usersRepository.GetUserProfile(userId);

            BrokerClient brokerClient = await dbContext.BrokerClient.FirstOrDefaultAsync(x => x.ClientId == userId);
            string feeType = this.GetFeeId(tradeType);
            if (brokerClient == null)
            {
                Formula formula = await GetFeeFormulaAsync(GetFeeId(tradeType), userProfile.FeeStructureId);
                Account feeAccount = transactionRepository.GetAccountByCurrencyId(userId, currencyId);
                FeeInfo fee = new FeeInfo(this.GetCollectors(userProfile.FeeStructureId, feeType, currencyId), feeAccount.AccountId, formula, feeType.ToString());
                return new FeeInfo[] { fee };
            }
            else
            {
                throw new Exception($"Broker fee is not YET supported. FeeModule.FeeRepository.{nameof(this.GetRealTradeFeeAsync)})");
                //ToDo: Сделать разделение fee на брокера и админа.
                //Formula adminFormula = await GetFeeFormula(GetFeeId(tradeType), userProfile.FeeStructureId);
                //if (adminFormula == null)
                //{
                //    adminFormula = await GetDefaultFee(GetFeeId(tradeType), userProfile);
                //}
                //CustomValidator.CheckNotNull(adminFormula, $"Does not exist fee formula for 'Exchange' and structure '{userProfile.FeeStructureId}'");
                //FeeInfo adminFee = new FeeInfo(AdminFeeAccount, adminFormula, userId, adminFormula.FormulaId);

                //Formula brokerFormula = await GetFeeFormula(GetFeeId(tradeType), userProfile.FeeStructureId);
                //if (brokerFormula == null)
                //{
                //    brokerFormula = await GetDefaultFee(GetFeeId(tradeType), userProfile);
                //}
                //CustomValidator.CheckNotNull(adminFormula, $"Does not exist fee formula for 'Exchange' and structure '{userProfile.FeeStructureId}'");
                //Account brokerAccount = transactionRepository.GetAccountByCurrencyId(brokerClient.BrokerId, Assets.UsdAssetId);
                //FeeInfo brokerFee = new FeeInfo(brokerAccount.AccountId, brokerFormula, userId, brokerFormula.FormulaId);

                //return new FeeInfo[] 
                //{
                //    adminFee,
                //    brokerFee
                //};
            }

        }

        public async Task<IEnumerable<FeeInfo>> GetZeroFeeAsync(string userId, TradeType tradeType, string currencyId)
        {
            UserProfile userProfile = await usersRepository.GetUserProfile(userId);
            string feeId = GetFeeId(tradeType);
            PercentFee simplePercentFee = new PercentFee(0m);
            Account feeAccount = transactionRepository.GetAccountByCurrencyId(userId, currencyId);
            CustomValidator.CheckNotNull(feeAccount, $"User {userProfile.ToName()} don't have {currencyId} account to collect fee. (FeeModule.FeeRepository.{nameof(this.GetZeroFeeAsync)})");
            return new FeeInfo[] { new FeeInfo(this.GetCollectors(userProfile.FeeStructureId, feeId, currencyId), feeAccount.AccountId, simplePercentFee, feeId) };
        }

        private string GetFeeId(TradeType tradeType)
        {
            return tradeType switch
            {
                TradeType.FixCoin => FeeType.FCOIN,
                TradeType.FixFiat => FeeType.FFIAT,
                TradeType.Limit => FeeType.LIMIT,
                _ => throw new Exception($"Unsupported trade type '{tradeType}'. ({nameof(FeeRepository)}.{nameof(GetFeeId)})"),
            };
        }

        /// <summary>
        /// Get active promo code.
        /// </summary>
        /// <param name="promoCode">Promo code id.</param>
        public Task<Promocode> GetActivePromoCodeAsync(string promoCode)
        {
            return dbContext.Promocode.FirstOrDefaultAsync(x => x.Active.HasValue && x.Active.Value && x.PromocodeId == promoCode);
        }

        /// <summary>
        /// Get titles.
        /// </summary>
        public IList<Title> GetTitles()
        {
            return titles;
        }

        /// <summary>
        /// Get titles.
        /// </summary>
        public IList<Group> GetGroups()
        {
            return dbContext.Group.ToList();
        }

        /// <summary>
        /// Get titles.
        /// </summary>
        public IList<DefaultStructure> GetDefaultStructures()
        {
            return dbContext.DefaultStructure.ToList();
        }
    }
}
