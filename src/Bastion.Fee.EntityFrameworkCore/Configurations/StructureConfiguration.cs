﻿namespace Bastion.Fee.EntityFrameworkCore.Configurations
{
    using Microsoft.EntityFrameworkCore;
    using Microsoft.EntityFrameworkCore.Metadata.Builders;
    using Bastion.Fee.Stores;

    internal class StructureConfiguration : IEntityTypeConfiguration<Structure>
    {
        public void Configure(EntityTypeBuilder<Structure> builder)
        {
            builder.ToTable("structure", "fee");

            builder.Property(e => e.StructureId)
                .HasColumnName("structure_id")
                .HasMaxLength(25);

            builder.Property(e => e.Name)
                .IsRequired()
                .HasColumnName("name")
                .HasMaxLength(255);

            builder.Property(e => e.ParentStructureId)
                .IsRequired()
                .HasColumnName("parent_structure_id")
                .HasMaxLength(25);

            builder.Property(e => e.PromoRuleId).HasColumnName("promo_rule_id");

            builder.HasOne(d => d.ParentStructure)
                .WithMany(p => p.InverseParentStructure)
                .HasForeignKey(d => d.ParentStructureId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("structure_parent_structure_id_fkey");
        }
    }
}