﻿namespace Bastion.Fee.EntityFrameworkCore.Configurations
{
    using Microsoft.EntityFrameworkCore;
    using Microsoft.EntityFrameworkCore.Metadata.Builders;
    using Bastion.Fee.Stores;

    internal class PromocodeConfiguration : IEntityTypeConfiguration<Promocode>
    {
        public void Configure(EntityTypeBuilder<Promocode> builder)
        {
            builder.ToTable("promocode", "fee");

            builder.Property(e => e.PromocodeId)
                .HasColumnName("promocode_id")
                .HasMaxLength(20);

            builder.Property(e => e.Active)
                .IsRequired()
                .HasColumnName("active")
                .HasDefaultValueSql("true");

            builder.Property(e => e.Description)
                .IsRequired()
                .HasColumnName("description")
                .HasMaxLength(255);

            builder.Property(e => e.ExpirationInterval)
                .IsRequired()
                .HasColumnName("expiration_interval")
                .HasMaxLength(255)
                .HasComment("for example,  \"10 days\" or \"6 months\" or \"1 year\"");

            builder.Property(e => e.StructureId)
                .IsRequired()
                .HasColumnName("structure_id")
                .HasMaxLength(25);

            builder.HasOne(d => d.Structure)
                .WithMany(p => p.Promocode)
                .HasForeignKey(d => d.StructureId)
                .HasConstraintName("promocode_structure_id_fkey");
        }
    }
}