﻿namespace Bastion.Fee.EntityFrameworkCore.Configurations
{
    using Microsoft.EntityFrameworkCore;
    using Microsoft.EntityFrameworkCore.Metadata.Builders;
    using Bastion.Fee.Stores;

    internal class RegionConfiguration : IEntityTypeConfiguration<Region>
    {
        public void Configure(EntityTypeBuilder<Region> builder)
        {
            builder.ToTable("region", "fee");

            builder.Property(e => e.RegionId)
                .HasColumnName("region_id")
                .HasMaxLength(15);
        }
    }
}