﻿namespace Bastion.Fee.EntityFrameworkCore.Configurations
{
    using Microsoft.EntityFrameworkCore;
    using Microsoft.EntityFrameworkCore.Metadata.Builders;
    using Bastion.Fee.Stores;

    internal class TitleConfiguration : IEntityTypeConfiguration<Title>
    {
        public void Configure(EntityTypeBuilder<Title> builder)
        {
            builder.ToTable("title", "fee");

            builder.HasIndex(e => e.SortOrder)
                .HasName("fee_sort_order_idx");

            builder.Property(e => e.TitleId)
                .HasColumnName("title_id")
                .HasMaxLength(25);

            builder.Property(e => e.Description)
                .HasColumnName("description")
                .HasMaxLength(255);

            builder.Property(e => e.GroupId)
                .IsRequired()
                .HasColumnName("group_id")
                .HasMaxLength(20);

            builder.Property(e => e.Name)
                .IsRequired()
                .HasColumnName("name")
                .HasMaxLength(255);

            builder.Property(e => e.SortOrder).HasColumnName("sort_order");

            builder.Property(e => e.Used)
                .HasColumnName("used")
                .HasMaxLength(255);

            builder.HasOne(d => d.Group)
                .WithMany(p => p.Title)
                .HasForeignKey(d => d.GroupId)
                .HasConstraintName("fee_group_id_fkey");
        }
    }
}