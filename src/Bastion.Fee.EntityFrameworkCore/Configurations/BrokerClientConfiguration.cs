﻿namespace Bastion.Fee.EntityFrameworkCore.Configurations
{
    using Microsoft.EntityFrameworkCore;
    using Microsoft.EntityFrameworkCore.Metadata.Builders;
    using Bastion.Fee.Stores;

    internal class BrokerClientConfiguration : IEntityTypeConfiguration<BrokerClient>
    {
        public void Configure(EntityTypeBuilder<BrokerClient> builder)
        {
            builder.HasKey(e => new { e.BrokerId, e.ClientId })
                .HasName("broker_client_pkey");

            builder.ToTable("broker_client", "baltika");

            builder.Property(e => e.BrokerId)
                .HasColumnName("broker_id")
                .HasMaxLength(36);

            builder.Property(e => e.ClientId)
                .HasColumnName("client_id")
                .HasMaxLength(36);
        }
    }
}