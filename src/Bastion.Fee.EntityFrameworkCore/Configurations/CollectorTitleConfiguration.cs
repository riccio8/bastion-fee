﻿namespace Bastion.Fee.EntityFrameworkCore.Configurations
{
    using Microsoft.EntityFrameworkCore;
    using Microsoft.EntityFrameworkCore.Metadata.Builders;
    using Bastion.Fee.Stores;

    internal class CollectorTitleConfiguration : IEntityTypeConfiguration<CollectorTitle>
    {
        public void Configure(EntityTypeBuilder<CollectorTitle> builder)
        {
            builder.HasKey(e => new { e.StructureId, e.TitleId })
                .HasName("collector_title_pkey");

            builder.ToTable("collector_title", "fee");

            builder.Property(e => e.StructureId)
                .HasColumnName("structure_id")
                .HasMaxLength(25)
                .HasDefaultValueSql("'default'::character varying");

            builder.Property(e => e.TitleId)
                .HasColumnName("title_id")
                .HasMaxLength(25);

            builder.HasOne(d => d.Structure)
                .WithMany(p => p.CollectorTitle)
                .HasForeignKey(d => d.StructureId)
                .HasConstraintName("collector_title_structure_id_fkey");

            builder.HasOne(d => d.Title)
                .WithMany(p => p.CollectorTitle)
                .HasForeignKey(d => d.TitleId)
                .HasConstraintName("collector_title_title_id_fkey");
        }
    }
}