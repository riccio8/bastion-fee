﻿namespace Bastion.Fee.EntityFrameworkCore.Configurations
{
    using Microsoft.EntityFrameworkCore;
    using Microsoft.EntityFrameworkCore.Metadata.Builders;
    using Bastion.Fee.Stores;

    internal class ClientTypeConfiguration : IEntityTypeConfiguration<ClientType>
    {
        public void Configure(EntityTypeBuilder<ClientType> builder)
        {
            builder.ToTable("client_type", "fee");

            builder.Property(e => e.ClientTypeId)
                .HasColumnName("client_type_id")
                .HasMaxLength(15);
        }
    }
}