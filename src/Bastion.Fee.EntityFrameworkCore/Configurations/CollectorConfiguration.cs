﻿namespace Bastion.Fee.EntityFrameworkCore.Configurations
{
    using Microsoft.EntityFrameworkCore;
    using Microsoft.EntityFrameworkCore.Metadata.Builders;
    using Bastion.Fee.Stores;

    internal class CollectorConfiguration : IEntityTypeConfiguration<Collector>
    {
        public void Configure(EntityTypeBuilder<Collector> builder)
        {
            builder.HasKey(e => new { e.StructureId, e.AccountId })
                .HasName("collector_pkey");

            builder.ToTable("collector", "fee");

            builder.Property(e => e.StructureId)
                .HasColumnName("structure_id")
                .HasMaxLength(25);

            builder.Property(e => e.AccountId)
                .HasColumnName("account_id")
                .HasMaxLength(20)
                .HasComment("our short account id, like SCA7H3");

            builder.Property(e => e.CurrencyId)
                .HasColumnName("currency_id")
                .HasMaxLength(3);

            builder.Property(e => e.Description)
                .HasColumnName("description")
                .HasMaxLength(255);

            builder.Property(e => e.Value)
                .HasColumnName("value")
                .HasColumnType("numeric(10,3)");

            builder.HasOne(d => d.Structure)
                .WithMany(p => p.Collector)
                .HasForeignKey(d => d.StructureId)
                .HasConstraintName("collector_structure_id_fkey");
        }
    }
}