﻿namespace Bastion.Fee.EntityFrameworkCore.Configurations
{
    using Microsoft.EntityFrameworkCore;
    using Microsoft.EntityFrameworkCore.Metadata.Builders;
    using Bastion.Fee.Stores;

    internal class DefaultStructureConfiguration : IEntityTypeConfiguration<DefaultStructure>
    {
        public void Configure(EntityTypeBuilder<DefaultStructure> builder)
        {
            builder.HasKey(e => new { e.ClientTypeId, e.FeeRegionId })
                .HasName("default_structure_pkey");

            builder.ToTable("default_structure", "fee");

            builder.Property(e => e.ClientTypeId)
                .HasColumnName("client_type_id")
                .HasMaxLength(15)
                .HasDefaultValueSql("'Individual'::character varying")
                .HasComment("Individual, Corporate");

            builder.Property(e => e.FeeRegionId)
                .HasColumnName("fee_region_id")
                .HasMaxLength(15)
                .HasDefaultValueSql("'NonEU'::character varying");

            builder.Property(e => e.DefaultStructureId)
                .IsRequired()
                .HasColumnName("default_structure_id")
                .HasMaxLength(25);

            builder.HasOne(d => d.ClientType)
                .WithMany(p => p.DefaultStructure)
                .HasForeignKey(d => d.ClientTypeId)
                .HasConstraintName("default_structure_client_type_id_fkey");

            builder.HasOne(d => d.DefaultStructureNavigation)
                .WithMany(p => p.DefaultStructure)
                .HasForeignKey(d => d.DefaultStructureId)
                .HasConstraintName("default_structure_default_structure_id_fkey");

            builder.HasOne(d => d.FeeRegion)
                .WithMany(p => p.DefaultStructure)
                .HasForeignKey(d => d.FeeRegionId)
                .HasConstraintName("default_structure_fee_region_id_fkey");
        }
    }
}