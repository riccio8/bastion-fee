﻿namespace Bastion.Fee.EntityFrameworkCore.Configurations
{
    using Microsoft.EntityFrameworkCore;
    using Microsoft.EntityFrameworkCore.Metadata.Builders;
    using Bastion.Fee.Stores;

    internal class FormulaConfiguration : IEntityTypeConfiguration<Formula>
    {
        public void Configure(EntityTypeBuilder<Formula> builder)
        {
            builder.HasKey(e => new { e.StructureId, e.TitleId })
                .HasName("formula_pkey");

            builder.ToTable("formula", "fee");

            builder.Property(e => e.StructureId)
                .HasColumnName("structure_id")
                .HasMaxLength(25)
                .HasDefaultValueSql("'debugFee'::character varying");

            builder.Property(e => e.TitleId)
                .HasColumnName("title_id")
                .HasMaxLength(25);

            builder.Property(e => e.Display)
                .HasColumnName("display")
                .HasColumnType("numeric(10,3)");

            builder.Property(e => e.Fixed)
                .HasColumnName("fixed")
                .HasColumnType("numeric(10,2)");

            builder.Property(e => e.Maximum)
                .HasColumnName("maximum")
                .HasColumnType("numeric(10,2)");

            builder.Property(e => e.Minimal)
                .HasColumnName("minimal")
                .HasColumnType("numeric(10,2)");

            builder.Property(e => e.Percent)
                .HasColumnName("percent")
                .HasColumnType("numeric(10,3)");

            builder.HasOne(d => d.Structure)
                .WithMany(p => p.Formula)
                .HasForeignKey(d => d.StructureId)
                .HasConstraintName("formula_structure_id_fkey");

            builder.HasOne(d => d.Title)
                .WithMany(p => p.Formula)
                .HasForeignKey(d => d.TitleId)
                .HasConstraintName("formula_fee_id_fkey");
        }
    }
}