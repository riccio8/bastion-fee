﻿namespace Bastion.Fee.EntityFrameworkCore.Configurations
{
    using Microsoft.EntityFrameworkCore;
    using Microsoft.EntityFrameworkCore.Metadata.Builders;
    using Bastion.Fee.Stores;

    internal class GroupConfiguration : IEntityTypeConfiguration<Group>
    {
        public void Configure(EntityTypeBuilder<Group> builder)
        {
            builder.ToTable("group", "fee");

            builder.HasIndex(e => e.SortOrder)
                .HasName("group_sort_order_idx");

            builder.Property(e => e.GroupId)
                .HasColumnName("group_id")
                .HasMaxLength(20);

            builder.Property(e => e.Name)
                .IsRequired()
                .HasColumnName("name")
                .HasMaxLength(255);

            builder.Property(e => e.SortOrder).HasColumnName("sort_order");
        }
    }
}