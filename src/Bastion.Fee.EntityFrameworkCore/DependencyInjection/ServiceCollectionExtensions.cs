﻿using Bastion.Fee.Abstractions;
using Bastion.Fee.EntityFrameworkCore.Contexts;
using Bastion.Fee.EntityFrameworkCore.Repositories;

using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection.Extensions;

using System;

namespace Microsoft.Extensions.DependencyInjection
{
    /// <summary>
    /// Service collection extensions for blocks repository and dbContext.
    /// </summary>
    public static class ServiceCollectionExtensions
    {
        /// <summary>
        /// Add blocks repository and dbContext.
        /// </summary>
        /// <param name="optionsAction">An optional action to configure the <see cref="DbContextOptions"/> for the context.</param>
        /// <param name="services">The <see cref="IServiceCollection"/> to add services to.</param>
        /// <returns>The same service collection so that multiple calls can be chained.</returns>
        public static IServiceCollection AddFeeRepository(
            this IServiceCollection services,
            Action<DbContextOptionsBuilder> optionsAction = null)
        {
            return services.AddFeeRepository<FeeRepository, FeeDbContext>(optionsAction);
        }

        /// <summary>
        /// Add custom blocks repository and dbContext.
        /// </summary>
        /// <typeparam name="TFeeRepository">The type of blocks repository to be registered.</typeparam>
        /// <typeparam name="TFeeDbContext">The type of context service to be registered.</typeparam>
        /// <param name="optionsAction">An optional action to configure the <see cref="DbContextOptions"/> for the context.</param>
        /// <param name="services">The <see cref="IServiceCollection"/> to add services to.</param>
        /// <returns>The same service collection so that multiple calls can be chained.</returns>
        public static IServiceCollection AddFeeRepository<TFeeRepository, TFeeDbContext>(
            this IServiceCollection services,
            Action<DbContextOptionsBuilder> optionsAction = null)
            where TFeeRepository : class, IFeeRepository
            where TFeeDbContext : FeeDbContext
        {
            services.AddDbContextPool<TFeeDbContext>(optionsAction);
            services.TryAddScoped<IFeeRepository, TFeeRepository>();

            return services;
        }
    }
}