﻿namespace Bastion.Fee.EntityFrameworkCore.Contexts
{
    using Bastion.Fee.EntityFrameworkCore.Configurations;
    using Bastion.Fee.Stores;
    using Bastion.Kernel;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.EntityFrameworkCore.InMemory;
    using System;

    /// <summary>
    /// Notification database context.
    /// </summary>
    public partial class FeeDbContext : DbContext
    {
        public FeeDbContext()
        {
        }

        internal string testDbName = null;
        public bool IsTestEnvironment { get { return testDbName != null; } }

        public FeeDbContext(string testDbName)
        {
            if (testDbName != null)
                this.testDbName = testDbName;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="FeeDbContext"/> class.
        /// </summary>
        public FeeDbContext(DbContextOptions<FeeDbContext> options)
            : base(options)
        {
        }
        public virtual DbSet<BrokerClient> BrokerClient { get; set; }
        public virtual DbSet<ClientType> ClientType { get; set; }
        public virtual DbSet<Collector> Collector { get; set; }
        public virtual DbSet<CollectorTitle> CollectorTitle { get; set; }
        public virtual DbSet<DefaultStructure> DefaultStructure { get; set; }
        public virtual DbSet<Formula> Formula { get; set; }
        public virtual DbSet<Group> Group { get; set; }
        public virtual DbSet<Promocode> Promocode { get; set; }
        public virtual DbSet<Region> Region { get; set; }
        public virtual DbSet<Structure> Structure { get; set; }
        public virtual DbSet<Title> Title { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!this.IsTestEnvironment)
            {
                if (!optionsBuilder.IsConfigured)
                {
                    var connectionString = CfgKernel.Configuration["DbConnection"];
                    if (connectionString == null)
                        throw new Exception("Missing config section DbConnection");
                    optionsBuilder.UseNpgsql(connectionString);
                }
            }
            else
            {
                optionsBuilder.UseInMemoryDatabase(testDbName);
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.ApplyConfiguration(new BrokerClientConfiguration());
            modelBuilder.ApplyConfiguration(new ClientTypeConfiguration());
            modelBuilder.ApplyConfiguration(new CollectorConfiguration());
            modelBuilder.ApplyConfiguration(new CollectorTitleConfiguration());
            modelBuilder.ApplyConfiguration(new DefaultStructureConfiguration());
            modelBuilder.ApplyConfiguration(new FormulaConfiguration());
            modelBuilder.ApplyConfiguration(new GroupConfiguration());
            modelBuilder.ApplyConfiguration(new PromocodeConfiguration());
            modelBuilder.ApplyConfiguration(new RegionConfiguration());
            modelBuilder.ApplyConfiguration(new StructureConfiguration());
            modelBuilder.ApplyConfiguration(new TitleConfiguration());
        }
    }
}
