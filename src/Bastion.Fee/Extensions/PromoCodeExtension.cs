﻿namespace Bastion.Fee.Extensions
{
    using Bastion.Fee.Stores;
    using System;

    /// <summary>
    /// PromoCode extension.
    /// </summary>
    public static class PromoCodeExtension
    {
        /// <summary>
        /// To dateTime.
        /// </summary>
        /// <param name="promoCode">Interval.</param>
        public static DateTime? FromInterval(this Promocode promoCode)
        {
            if (!string.IsNullOrEmpty(promoCode?.ExpirationInterval))
            {
                var indexSeparator = promoCode.ExpirationInterval.IndexOf(' ');
                if (indexSeparator > -1 && promoCode.ExpirationInterval.Length > indexSeparator 
                    && int.TryParse(promoCode.ExpirationInterval.Substring(0, indexSeparator), out int value))
                {
                    switch (promoCode.ExpirationInterval.Substring(indexSeparator + 1).ToUpper())
                    {
                        case "HOUR":
                        case "HOURS":
                            return DateTime.UtcNow.AddHours(value);
                        case "DAY":
                        case "DAYS":
                            return DateTime.UtcNow.AddDays(value);
                        case "MONTH":
                        case "MONTHS":
                            return DateTime.UtcNow.AddMonths(value);
                        case "YEAR":
                        case "YEARS":
                            return DateTime.UtcNow.AddYears(value);
                    }
                }
            }
            return default;
        }
    }
}
