﻿namespace Bastion.Fee
{
    /// !!!!!!!!!!TABLE FEE.TITLE!!!!!!!!!!!!!!<summary>
    /// *******************
    /// *******************</summary>
    public static class FeeType
    {
        public const string MON = "MON";            // ACCOUNT_MANAGEMENT   Ежемесячный платеж за пользование счётом
        public const string ANUAL = "ANUAL";        // ACCOUNT_MANAGEMENT   Ежегодный платеж за пользование счётом
        public const string OVRDRFT_PROTECT = "OVRDRFT_PROTECT"; // ACCOUNT_MANAGEMENT  Плата за прохождение транзакции покупки даже если деньги на счету ушли в минус(если счёт БЫЛ защищен от overdraft)
        public const string OVRDRFT = "OVRDRFT";    // ACCOUNT_MANAGEMENT   Плата за уход в минус(если счёт НЕ был защищен от overdraft)
        public const string DEPBNK = "DEPBNK";      // TRANSFER_IN          Плата за получение денег на счёт(за депозит) из внешнего банка(с помощью оператора??)
        public const string DEPVOU = "DEPVOU";      // TRANSFER_IN          Обналичивание некого ваучера
        public const string DEPCRD = "DEPCRD";      // TRANSFER_IN          Депозит с помощью карты(виза или мастеркарт)
        public const string DEPINS = "DEPINS";      // TRANSFER_IN          Депозит с помощью некого интернет-перевода прямо на счёт
        public const string DEPATM = "DEPATM";      // TRANSFER_IN          Депозит через банкомат
        public const string WDREU = "WDREU";        // TRANSFER_OUT         Перевод из банка на внешний банковский счет в пределах ЕС
        public const string WDRNEU = "WDRNEU";      // TRANSFER_OUT         Перевод из банка на внешний банковский счет вне ЕС
        public const string TRNSF = "TRNSF";        // TRANSFER_OUT         Внутренний перевод, на собственный счет
        public const string TOUSR = "TOUSR";        // TRANSFER_OUT         Внутренний перевод, другому пользователю системы
        public const string BGPG = "BGPG";          // TRANSFER_OUT         Оплатна некой "банковской гарантии"
        public const string ACTIV = "ACTIV";        // CARD_MANAGEMENT      Плата за активацию карточки
        public const string CMON = "CMON";          // CARD_MANAGEMENT      Ежемесячный платеж за пользование картой
        public const string REPL = "REPL";          // CARD_MANAGEMENT      Замена карты
        public const string ATMEU = "ATMEU";        // CARD_TRANSACTIONS    Снятие наличных в банкоматах ЕС
        public const string ATMNEU = "ATMNEU";      // CARD_TRANSACTIONS    Снятие наличных в банкоматах вне ЕС
        public const string BALAN = "BALAN";        // CARD_TRANSACTIONS    Просмотр своего баланса в банкомате
        public const string DECLI = "DECLI";        // CARD_TRANSACTIONS    Отказ обслуживания банкоматом
        public const string CURR = "CURR";          // CARD_TRANSACTIONS    Обмен валюты
        public const string POSEU = "POSEU";        // CARD_TRANSACTIONS    Покупка в магазине(внутри ЕС)
        public const string POSNEU = "POSNEU";      // CARD_TRANSACTIONS    Покупка в магазине(вне ЕС)
        public const string POSDEC = "POSDEC";      // CARD_TRANSACTIONS    отказ в обслуживании в магазине
        public const string TXTBAL = "TXTBAL";      // CARD_TRANSACTIONS    проверка баланса по СМС
        public const string TXTBL = "TXTBL";        // CARD_TRANSACTIONS    блокировка счёта по СМС
        public const string TXTUNBL = "TXTUNBL";    // CARD_TRANSACTIONS    разблокировка счёта по СМС
        public const string TXTACT = "TXTACT";      // CARD_TRANSACTIONS    Активация СМС-контроля за счётом
        public const string TXTPIN = "TXTPIN";      // CARD_TRANSACTIONS    запрос ПИН-кода по СМС
        public const string FFIAT = "FFIAT";        // DIMM_TRADING         Торговля используя фиксированную сумму денег
        public const string FCOIN = "FCOIN";        // DIMM_TRADING         Торговля используя фиксированное количество монет
        public const string LIMIT = "LIMIT";        // DIMM_TRADING         Торговля используя limit price и фиксированное количество монет
        public const string OUTPSRCH = "OUTPSRCH";  // ADDITIONAL_SERVICES	"поиск платежа"  в нашей базе нашим оператором
        public const string OUTPRET = "OUTPRET";    // ADDITIONAL_SERVICES  возврат платежа -- ушел в никуда и вернулся в итоге
        public const string INPSRCH = "INPSRCH";    // ADDITIONAL_SERVICES  поиск входящего платежа нашим оператором
        public const string ADMIN_TRANSFER = "ADMIN_TRANSFER"; //  ADDITIONAL_SERVICES админ переводит деньги со счета пользователя на свой или обратно
        public const string ANY = "ANY"; // UNKNOWN
        public const string UNK = "UNK"; // OLD_VERSION
    }
}
