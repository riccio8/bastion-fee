﻿namespace Bastion.Fee.Constants
{
    public static class FeeRegionConstants
    {
        public const string EU = "EU";
        public const string NonEU = "NonEU";
    }
}
