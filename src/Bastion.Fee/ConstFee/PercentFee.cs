﻿namespace Bastion.Fee.ConstFee
{
    using Bastion.Accounts.Interfaces;

    public class PercentFee : IFeeCalculate
    {
        private decimal percent;

        public PercentFee(decimal? percent)
        {
            this.percent = percent ?? decimal.Zero;
        }
        public decimal Calculate(decimal amount)
        {
            return amount * percent / 100m;
        }
    }
}
