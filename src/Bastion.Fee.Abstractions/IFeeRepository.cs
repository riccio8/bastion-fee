﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Bastion.Accounts.Interfaces;
using Bastion.Accounts.Models;
using Bastion.Fee.Stores;

namespace Bastion.Fee.Abstractions
{
    public interface IFeeRepository
    {
        Task<Formula> GetFeeFormulaAsync(string feeType, string structureId);
        Task<Promocode> GetActivePromoCodeAsync(string promoCode);
        Task<decimal> GetAvailableBalanceForWithdrawAsync(string accountId, string currencyId, Func<Account, bool> accountFilter);
        IEnumerable<CollectorInfo> GetCollectors(string structureId, string feeType, string currencyId);
        Task<Dictionary<string, Formula>> GetFeeAsync(string accountId, string currencyId);
        Task<IEnumerable<FeeInfo>> GetFeeAsync(string feeType, string currencyId, Func<Account, bool> accountFilter, params string[] accountsId);
        Task<Formula> GetFeeByUserIdAsync(string feeType, string userId);
        Task<IList<Structure>> GetStructuresAsync();
        Task<FeeInfo> GetDisplayTradeFeeAsync(string userId, TradeType tradeType, string currencyId);
        Task<IEnumerable<FeeInfo>> GetRealTradeFeeAsync(string userId, TradeType tradeType, string currencyId);
        Task<IEnumerable<FeeInfo>> GetZeroFeeAsync(string userId, TradeType tradeType, string currencyId);
        IList<Title> GetTitles();
        IList<Group> GetGroups();
        IList<DefaultStructure> GetDefaultStructures();
    }
}