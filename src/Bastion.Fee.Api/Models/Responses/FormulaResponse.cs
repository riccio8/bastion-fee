﻿namespace Bastion.Fee.Api.Models.Responses
{
    using Bastion.Fee.Stores;
    using Newtonsoft.Json;

    public class FormulaResponse
    {
        public string TitleId { get; set; }
        public string Name { get; set; }
        public decimal? Fixed { get; set; }
        public decimal? Minimal { get; set; }
        public decimal? Maximum { get; set; }
        public decimal? Percent { get; set; }
        public decimal? Display { get; set; }
        public bool IsOverride { get; set; }
        [JsonIgnore]
        public short SortOrder { get; set; }

        public FormulaResponse()
        {

        }

        public FormulaResponse(Formula formula, bool isOverride)
        {
            this.TitleId = formula.TitleId;
            this.Fixed = formula.Fixed;
            this.Minimal = formula.Minimal;
            this.Maximum = formula.Maximum;
            this.Percent = formula.Percent;
            this.Display = formula.Display;
            this.IsOverride = isOverride;
            this.Name = formula.Title.Name;
            this.SortOrder = formula.Title.SortOrder;
        }

        public FormulaResponse(FormulaResponse parentFormula, bool isOverride)
        {
            this.TitleId = parentFormula.TitleId;
            this.Fixed = parentFormula.Fixed;
            this.Minimal = parentFormula.Minimal;
            this.Maximum = parentFormula.Maximum;
            this.Percent = parentFormula.Percent;
            this.Display = parentFormula.Display;
            this.IsOverride = isOverride;
            this.Name = parentFormula.Name;
            this.SortOrder = parentFormula.SortOrder;
        }
    }
}
