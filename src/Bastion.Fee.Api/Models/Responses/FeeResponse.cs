﻿namespace Bastion.Fee.Api.Models.Responses
{
    using System.Collections.Generic;
    using System.Linq;
    using Bastion.Fee.Stores;

    public class FeeResponse
    {
        public string StructureId { get; set; }
        public string Name { get; set; }
        public IList<FeeResponse> Children { get; set; }
        public IDictionary<string, List<FormulaResponse>> Formulas { get; set; }

        public FeeResponse()
        {
            this.Children = new List<FeeResponse>();
        }

        public FeeResponse(Structure structure, FeeResponse parentFeeResponse = null) : this()
        {
            this.StructureId = structure.StructureId;
            this.Name = structure.Name;
            this.Formulas = structure.Formula
                .GroupBy(x => x.Title.Group.Name)
                .ToDictionary(x => x.Key, x => x.Select(y => new FormulaResponse(y, true)).ToList());

            if (parentFeeResponse != null)
            {
                foreach (var groupFormula in parentFeeResponse.Formulas)
                {
                    foreach (var parentFormula in groupFormula.Value)
                    {
                        if (this.Formulas.Values.FirstOrDefault(x => x.Any(y => y.TitleId == parentFormula.TitleId)) == null)
                        {
                            FormulaResponse fr = new FormulaResponse(parentFormula, false);
                            if (this.Formulas.ContainsKey(groupFormula.Key))
                            {
                                this.Formulas[groupFormula.Key].Add(fr);
                            }
                            else
                            {
                                this.Formulas.Add(groupFormula.Key, new List<FormulaResponse> { fr });
                            }
                        }
                    }
                }
            }
            foreach (var item in this.Formulas.Keys.ToList())
            {
                this.Formulas[item] = this.Formulas[item].OrderBy(x => x.SortOrder).ToList();
            }
            foreach (Structure childStructure in structure.InverseParentStructure.Where(x => x.StructureId != structure.StructureId))
            {
                Children.Add(new FeeResponse(childStructure, this));
            }
        }

        public static void OrderByGroup(FeeResponse response, IList<Group> groups)
        {
            response.Formulas = response.Formulas
                .OrderBy(x => groups.First(y => y.Name == x.Key).SortOrder)
                .ToDictionary(x => x.Key, x => x.Value);
            foreach (var childResponse in response.Children)
            {
                OrderByGroup(childResponse, groups);
            }
        }
    }
}
