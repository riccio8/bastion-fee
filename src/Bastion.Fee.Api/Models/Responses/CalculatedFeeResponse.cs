﻿namespace Bastion.Fee.Api.Models.Responses
{
    public class CalculatedFeeResponse
    {
        public decimal Fee { get; set; }

        public CalculatedFeeResponse(decimal fee)
        {
            this.Fee = fee;
        }
    }
}