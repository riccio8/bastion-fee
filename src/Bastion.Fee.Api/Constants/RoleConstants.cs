﻿namespace Bastion.Fee.Api.Constants
{
    internal static class RoleConstants
    {
        public const string Admin = "Admin";
        public const string Regulator = "Regulator";

        public const string Clerk = "Clerk";
        public const string Operator = "Operator";
        public const string Controller = "Controller";

        public const string Client = "Client";
        public const string Corporate = "Corporate";
        public const string CorporateWaitForApproval = "WaitForApproval";

        public const string AdminOrRegulator = "Admin, Regulator";
        public const string ClerkOrControllerOrOperatorOrAdmin = "Clerk, Controller, Operator, Admin";
        public const string ClerkOrControllerOrOperatorOrRegulatorOrAdmin = "Clerk, Controller, Operator, Regulator, Admin";
    }
}
