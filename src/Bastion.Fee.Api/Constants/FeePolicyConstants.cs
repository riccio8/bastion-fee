﻿namespace Bastion.Fee.Api.Constants
{
    /// <summary>
    /// Fee policy constants.
    /// </summary>
    public static class FeePolicyConstants
    {
        /// <summary>
        /// Full access.
        /// </summary>
        public const string FullAccess = "BlocksFullAccess";
    }
}