﻿namespace Bastion.Fee.Api.Controllers.Administrator
{
    using Bastion.Fee.Abstractions;
    using Bastion.Fee.Api.Constants;
    using Bastion.Fee.Api.Models.Responses;
    using Bastion.Fee.Stores;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;

    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;

    /// <summary>
    /// Fees controller
    /// </summary>
    [Authorize]
    [ApiController]
    [Produces("application/json")]
    [Route("api/v1/[controller]")]
    public class FeesController : ControllerBase
    {
        private readonly IFeeRepository feeRepository;
        private const string defaultStructure = "default";

        public FeesController(
            IFeeRepository feeRepository)
        {
            this.feeRepository = feeRepository;
        }

        [HttpGet]
        [Authorize(Roles = RoleConstants.AdminOrRegulator)]
        public async Task<ActionResult<FeeResponse>> GetFeesAsync()
        {
            IList<Structure> structures = await feeRepository.GetStructuresAsync();
            FeeResponse resp = new FeeResponse(structures.First(x => x.StructureId == defaultStructure));
            IList<Group> groups = feeRepository.GetGroups();
            FeeResponse.OrderByGroup(resp, groups);
            return resp;
        }
    }
}