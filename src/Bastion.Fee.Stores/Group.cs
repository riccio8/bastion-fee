﻿using System;
using System.Collections.Generic;

namespace Bastion.Fee.Stores
{
    public partial class Group
    {
        public Group()
        {
            Title = new HashSet<Title>();
        }

        public string GroupId { get; set; }
        public string Name { get; set; }
        public short? SortOrder { get; set; }

        public virtual ICollection<Title> Title { get; set; }
    }
}
