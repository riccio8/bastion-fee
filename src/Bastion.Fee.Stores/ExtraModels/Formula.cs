﻿using Bastion.Accounts.Interfaces;
using Bastion.Helper;
using System;

namespace Bastion.Fee.Stores
{
    public partial class Formula : IFeeCalculate
    {
        public decimal Calculate(decimal amount)
        {
            if (this.Percent == null)
            {
                CustomValidator.CheckNotNull(this.Fixed, $"Not set comission for structure '{StructureId}' and fee '{TitleId}'");
                return (decimal)this.Fixed;
            }

            var feeValue = amount * (decimal)Percent / 100;
            if (this.Fixed != null)
            {
                feeValue = Math.Max(feeValue, (decimal)this.Fixed);
            }
            if (this.Minimal != null)
            {
                feeValue = Math.Max(feeValue, (decimal)this.Minimal);
            }
            if (this.Maximum != null)
            {
                feeValue = Math.Min(feeValue, (decimal)this.Maximum);
            }
            return decimal.Round(feeValue, 2);
        }
    }
}