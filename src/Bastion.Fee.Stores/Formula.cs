﻿using System;
using System.Collections.Generic;

namespace Bastion.Fee.Stores
{
    public partial class Formula
    {
        public string StructureId { get; set; }
        public string TitleId { get; set; }
        public decimal? Fixed { get; set; }
        public decimal? Minimal { get; set; }
        public decimal? Maximum { get; set; }
        public decimal? Percent { get; set; }
        public decimal? Display { get; set; }

        public virtual Structure Structure { get; set; }
        public virtual Title Title { get; set; }
    }
}
