﻿using System.Collections.Generic;

namespace Bastion.Fee.Stores
{
    public partial class ClientType
    {
        public ClientType()
        {
            DefaultStructure = new HashSet<DefaultStructure>();
        }

        public string ClientTypeId { get; set; }

        public virtual ICollection<DefaultStructure> DefaultStructure { get; set; }
    }
}
