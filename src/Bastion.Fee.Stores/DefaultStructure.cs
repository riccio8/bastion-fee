﻿using System;
using System.Collections.Generic;

namespace Bastion.Fee.Stores
{
    public partial class DefaultStructure
    {
        public string ClientTypeId { get; set; }
        public string FeeRegionId { get; set; }
        public string DefaultStructureId { get; set; }

        public virtual ClientType ClientType { get; set; }
        public virtual Structure DefaultStructureNavigation { get; set; }
        public virtual Region FeeRegion { get; set; }
    }
}
