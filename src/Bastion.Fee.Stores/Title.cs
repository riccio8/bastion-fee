﻿using System;
using System.Collections.Generic;

namespace Bastion.Fee.Stores
{
    public partial class Title
    {
        public Title()
        {
            CollectorTitle = new HashSet<CollectorTitle>();
            Formula = new HashSet<Formula>();
        }

        public string TitleId { get; set; }
        public string Name { get; set; }
        public string GroupId { get; set; }
        public short SortOrder { get; set; }
        public string Used { get; set; }
        public string Description { get; set; }

        public virtual Group Group { get; set; }
        public virtual ICollection<CollectorTitle> CollectorTitle { get; set; }
        public virtual ICollection<Formula> Formula { get; set; }
    }
}
