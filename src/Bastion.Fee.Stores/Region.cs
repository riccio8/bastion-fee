﻿using System;
using System.Collections.Generic;

namespace Bastion.Fee.Stores
{
    public partial class Region
    {
        public Region()
        {
            DefaultStructure = new HashSet<DefaultStructure>();
        }

        public string RegionId { get; set; }

        public virtual ICollection<DefaultStructure> DefaultStructure { get; set; }
    }
}
