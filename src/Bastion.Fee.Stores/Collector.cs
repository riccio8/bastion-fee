﻿using System;
using System.Collections.Generic;

namespace Bastion.Fee.Stores
{
    public partial class Collector
    {
        public string StructureId { get; set; }
        public string AccountId { get; set; }
        public decimal Value { get; set; }
        public string CurrencyId { get; set; }
        public string Description { get; set; }

        public virtual Structure Structure { get; set; }
    }
}
