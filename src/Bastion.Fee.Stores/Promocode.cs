﻿using System;
using System.Collections.Generic;

namespace Bastion.Fee.Stores
{
    public partial class Promocode
    {
        public string PromocodeId { get; set; }
        public string StructureId { get; set; }
        public string ExpirationInterval { get; set; }
        public bool? Active { get; set; }
        public string Description { get; set; }

        public virtual Structure Structure { get; set; }
    }
}
