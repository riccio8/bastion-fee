﻿using System;
using System.Collections.Generic;

namespace Bastion.Fee.Stores
{
    public partial class BrokerClient
    {
        public string BrokerId { get; set; }
        public string ClientId { get; set; }
    }
}
