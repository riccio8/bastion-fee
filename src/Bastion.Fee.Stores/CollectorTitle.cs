﻿using System;
using System.Collections.Generic;

namespace Bastion.Fee.Stores
{
    public partial class CollectorTitle
    {
        public string StructureId { get; set; }
        public string TitleId { get; set; }

        public virtual Structure Structure { get; set; }
        public virtual Title Title { get; set; }
    }
}
