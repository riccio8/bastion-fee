﻿using System;
using System.Collections.Generic;

namespace Bastion.Fee.Stores
{
    public partial class Structure
    {
        public Structure()
        {
            Collector = new HashSet<Collector>();
            CollectorTitle = new HashSet<CollectorTitle>();
            DefaultStructure = new HashSet<DefaultStructure>();
            Formula = new HashSet<Formula>();
            InverseParentStructure = new HashSet<Structure>();
            Promocode = new HashSet<Promocode>();
        }

        public string StructureId { get; set; }
        public string ParentStructureId { get; set; }
        public string Name { get; set; }
        public int? PromoRuleId { get; set; }

        public virtual Structure ParentStructure { get; set; }
        public virtual ICollection<Collector> Collector { get; set; }
        public virtual ICollection<CollectorTitle> CollectorTitle { get; set; }
        public virtual ICollection<DefaultStructure> DefaultStructure { get; set; }
        public virtual ICollection<Formula> Formula { get; set; }
        public virtual ICollection<Structure> InverseParentStructure { get; set; }
        public virtual ICollection<Promocode> Promocode { get; set; }
    }
}
