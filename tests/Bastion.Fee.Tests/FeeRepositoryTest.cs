﻿namespace Bastion.Fee.Tests
{
    using Bastion.Accounts;
    using Bastion.Accounts.Context;
    using Bastion.Accounts.Models;
    using Bastion.Accounts.Stores;
    using Bastion.Fee.Abstractions;
    using Bastion.Fee.EntityFrameworkCore.Contexts;
    using Bastion.Fee.EntityFrameworkCore.Repositories;
    using Bastion.Fee.Stores;
    using Bastion.Users.Models;
    using Bastion.Users.Services;
    using Microsoft.EntityFrameworkCore;
    using System;
    using System.Linq;

    public class FeeRepositoryTest
    {
        private static readonly Random random = new Random();
        private static readonly AccountsStore accountsStore = new AccountsStore();
        private static readonly CurrencyStore currencyStore = new CurrencyStore();

        public static string RandomString(int length)
        {
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            return new string(Enumerable.Repeat(chars, length)
              .Select(s => s[random.Next(s.Length)]).ToArray());
        }

        public IFeeRepository GetFeeRepository(string dbName = null)
        {
            if (dbName == null)
                dbName = RandomString(10); // если не передали имя тестовой базы, сгенерируем случайное имя

            var transactionRepository = new TransactionRepository(accountsStore, currencyStore, new AccountContext(dbName));
            transactionRepository.AddEntities(AccountContext.GetTransactionStateData()).Wait();
            transactionRepository.AddEntities(AccountContext.GetTransactionTypeData()).Wait();
            transactionRepository.AddEntities(GetTestCurrency()).Wait();
            transactionRepository.AddEntities(GetTestAccounts()).Wait();
            UsersRepository usersRepository = null;
            //Сломано сейчас
            //new UsersRepository(new UsersContext(new DbContextOptions<UsersContext>(options => options.UseInMemoryDatabase(testDbName));
            var feeContext = new FeeDbContext(dbName);
            feeContext.Formula.AddRange(GetTestFormulas());
            feeContext.Title.AddRange(GetTestTitles());
            feeContext.Structure.AddRange(GetTestStructures());
            feeContext.SaveChanges();
            
            var feeRepository =  new FeeRepository(transactionRepository, usersRepository, feeContext);
            return feeRepository;
        }

        private static Currency[] GetTestCurrency()
        {
            return new Currency[]
            {
                new Currency()
                {
                    CurrencyId = "EUR",
                    DecimalPoints = 2,
                    Formatter = "N2"
                },
                new Currency()
                {
                    CurrencyId = "BTC",
                    DecimalPoints = 8,
                    Formatter = "N8"
                },
                new Currency()
                {
                    CurrencyId = "USD",
                    DecimalPoints = 2,
                    Formatter = "N2"
                },
            };
        }

        private static Account[] GetTestAccounts()
        {
            return new Account[]
            {
                new Account()
                {
                    AccountId = "TestAccount1",
                    CurrencyId = "EUR",
                    UserId = "TestUser1",
                    AccountNumber = "TestAccountNumber1",
                    AccountTypeId = "Migom"
                },
                new Account()
                {
                    AccountId = "TestAccount2",
                    CurrencyId = "EUR",
                    UserId = "TestUser2",
                    AccountNumber = "TestAccountNumber2",
                    AccountTypeId = "Migom"
                },
                new Account()
                {
                    AccountId = "TestAccount3",
                    CurrencyId = "BTC",
                    UserId = "TestUser1",
                    AccountNumber = "TestAccountNumber3",
                    AccountTypeId = "DIMM"
                },
                new Account()
                {
                    AccountId = "TestUser1External",
                    CurrencyId = "EUR",
                    UserId = "TestUser1",
                    AccountNumber = "ExternalAccount",
                    AccountTypeId = "External"
                },
                new Account()
                {
                    AccountId = "TestFeeAccount1",
                    CurrencyId = "EUR",
                    UserId = "TestFeeUser1",
                    AccountNumber = "TestFeeAccountNumber1",
                    AccountTypeId = "Migom"
                },
                new Account()
                {
                    AccountId = "TestFeeAccount2",
                    CurrencyId = "EUR",
                    UserId = "TestFeeUser2",
                    AccountNumber = "TestFeeAccountNumber2",
                    AccountTypeId = "Migom"
                },
            };
        }

        private static Formula[] GetTestFormulas()
        {
            return new Formula[]
            {
                new Formula()
                {
                    Display = 1m,
                    Fixed = 2m,
                    Minimal = 3m,
                    Maximum = 4m,
                    Percent = 5m,
                    StructureId = "TestStructure",
                    TitleId = "TestTitleId"
                }
            };
        }

        private static Structure[] GetTestStructures()
        {
            return new Structure[]
            {
                new Structure()
                {
                    Name = "TestStructureName",
                    StructureId = "A"
                }
            };
        }

        private static Title[] GetTestTitles()
        {
            return new Title[]
            {
                new Title()
                {
                    TitleId = "TestTitleId",
                    GroupId = "TestGroupId"
                }
            };
        }
    }
}