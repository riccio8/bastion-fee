﻿using Bastion.Accounts.Models;
using Bastion.Fee.Abstractions;
using Bastion.Fee.Stores;
using System;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace Bastion.Fee.Tests
{
    public class TitleTest : FeeRepositoryTest
    {
        public TitleTest()
        {
        }

        [Fact]
        public void GetTitleTest()
        {
            IFeeRepository feeRepository = GetFeeRepository();
            IList<Title> title = feeRepository.GetTitles();
            Assert.NotNull(title);
            int count = title.Count;
            Assert.Equal(1, count);
            Assert.Equal("TestTitleId", title.First().TitleId);
        }
    }
}