using Bastion.Accounts;
using Bastion.Accounts.Context;
using Bastion.Accounts.Stores;
using Bastion.Fee.Abstractions;
using Bastion.Fee.Api.Constants;
using Bastion.Fee.EntityFrameworkCore.Contexts;
using Bastion.Fee.EntityFrameworkCore.Repositories;
using Bastion.Kernel;
using Bastion.Users.Services;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.OpenApi.Models;

using Swashbuckle.AspNetCore.Filters;

using System;
using System.IO;
using System.Linq;

namespace BastionFeeSample.Api
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<AccountContext>(options =>
            {
                options.UseNpgsql(CfgKernel.Configuration["DbConnection"]);
                options.UseLazyLoadingProxies();
            });
            services.AddSingleton<AccountsStore>();
            services.AddSingleton<CurrencyStore>();
            services.AddScoped<TransactionRepository>();
            //services.AddFeeService();
            services.AddScoped<UsersRepository>();

            // ===== Add Bastion Fee ========
            services.AddDbContext<FeeDbContext>(options =>
            {
                options.UseNpgsql(CfgKernel.Configuration["DbConnection"]);
                options.UseLazyLoadingProxies();
            });
            services.AddScoped<IFeeRepository, FeeRepository>();

            services.AddControllers();

            services.AddSwaggerGen(options =>
            {
                var version = typeof(FeePolicyConstants).Assembly.GetName().Version;

                options.SwaggerDoc("v1", new OpenApiInfo
                {
                    Title = "Bastion fee API",
                    Version = "v1",
                    Description = $"Version: <b>{version.ToString()}</b>",
                });

                Directory.GetFiles(AppContext.BaseDirectory, "*.xml").ToList().ForEach(xmlFilePath => options.IncludeXmlComments(xmlFilePath));

                options.OperationFilter<AppendAuthorizeToSummaryOperationFilter>();
                /*
                options.AddSecurityDefinition(BasicAuthenticationDefaults.AuthenticationScheme, new OpenApiSecurityScheme()
                {
                    Name = "Authorization",
                    In = ParameterLocation.Header,
                    Type = SecuritySchemeType.Http,
                    Scheme = BasicAuthenticationDefaults.AuthenticationScheme,
                    Description = "Input your username and password to access this API",
                });
                options.OperationFilter<SecurityRequirementsOperationFilter>(true, BasicAuthenticationDefaults.AuthenticationScheme);
                */
                options.AddSecurityDefinition(JwtBearerDefaults.AuthenticationScheme, new OpenApiSecurityScheme
                {
                    Name = "Authorization",
                    In = ParameterLocation.Header,
                    Type = SecuritySchemeType.ApiKey,
                    Scheme = JwtBearerDefaults.AuthenticationScheme,
                    Description = "JWT Authorization header using the Bearer scheme.<br/>Example: \"<b>Bearer</b> {token}\""
                });
                options.OperationFilter<SecurityRequirementsOperationFilter>(false, JwtBearerDefaults.AuthenticationScheme);
            });
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseRouting();

            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });

            app.UseSwagger().UseSwaggerUI(options =>
            {
                options.RoutePrefix = string.Empty;
                options.SwaggerEndpoint("/swagger/v1/swagger.json", "Bastion fee API V1");
            });
        }
    }
}